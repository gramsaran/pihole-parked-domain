A personal list of invalid or parked domains to be added to Pi-hole to be blocked.
Domains are to be blocked because they are not valid or incorrectly spelled and to stop me from attempting to connect to them.
